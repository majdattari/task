import 'package:flutter/material.dart';
import 'package:majd_task/provider/provider.dart';
import 'package:majd_task/screens/home_screen.dart';
import 'package:majd_task/screens/login_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'model/user_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences appLang = await SharedPreferences.getInstance();
  if (appLang.getBool('userlogin') == null) appLang.setBool('userlogin', false);

  bool isLogin = appLang.getBool('userlogin');

  Widget HomeWidget = new MediaQuery(
    data: new MediaQueryData(),
    child: new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new HomeScreen(),
    ),
  );

  runApp(ChangeNotifierProvider<MyProvider>(
    create: (_) => MyProvider(),
    child: isLogin == false ? MyApp() : HomeWidget,
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    UserData myUser = Provider.of<MyProvider>(context).user;

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          accentColor: Color(0xff0f5382),
          textTheme: ThemeData.light().textTheme.copyWith(
                body1: TextStyle(
                  fontSize: 15,
                  color: Colors.blueGrey,
                ),
                body2: TextStyle(
                  fontSize: 15,
                  color: Color(0xff0f5382),
                ),
              )),
      home: myUser.Urltoken == null || myUser.Urltoken == ""
          ? LoginScreen()
          : HomeScreen(),
    );
  }
}
