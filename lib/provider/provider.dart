import 'package:flutter/material.dart';
import 'package:majd_task/database/database.dart';
import 'package:majd_task/model/user_data.dart';

class MyProvider with ChangeNotifier {
  UserData user= UserData.fromJson({});

  void addUser(Map<String, dynamic> map) {
    user = UserData.fromJson(map);
    notifyListeners();
  }

  void deleteUser() {
    user = UserData.fromJson({});
    notifyListeners();
  }


}
