import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:majd_task/model/user_data.dart';

Future login(String username, String password) async {
  String url = "http://82.212.90.190:7777/rest/OmanAPI_DEV/MobileAPI";

  var result;
  UserData userData;
  await http.post(Uri.parse(url), headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie':
    'CFID=1075314; CFTOKEN=fd56b20f01fb1a3c-9CD7E287-E785-21CC-DE5B5B6AF59E269E; JSESSIONID=E11F8A190FE627EA1CF790B4306F6730.cfusion'
  }, body: {
    'args':
    '{"ENTITY":"CONFIG","ACTION":"LOGIN","DATA":{"USER":{"SESSION":"2","URLTOKEN":"","USER_ID":"","USER_SOURCE":"EXTERNAL","LANGUAGE":1},"data":{"APPLICATION_NAME":"OMANTCS","LANGUAGE":"1","PASSWORD":"${password}","REMEMBER_ME":"0","USERNAME":"${username}","USER_DEVICE_INFO":{"APPLICATION_VERSION":"2.0.0","DEVICE_ID":"649F60AC-C952-4DFF-B518-1E75DFBDF6B5","DEVICE_IN_INCH":"test","DEVICE_MAC_ADDRESS":"GetMac.macAddress","DEVICE_MANUFACTURE":"APPLE","DEVICE_MODEL":"iPhone","DEVICE_OS":"Darwin","DEVICE_OS_VERSION":"12.5.4","FCM_TOKEN":"7a667cfd-ae2c-4c7c-9aca-636eb6b0b934"}}}}'
  }).then((value) {
    result = jsonDecode(value.body);
  });

  return result;
}
