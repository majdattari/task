import 'package:majd_task/model/user_data.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class MyDatabase {
  Future<Database> database() async {
    return openDatabase(
      join(await getDatabasesPath(), 'majdtask.db'),
      onCreate: (db, version) async {
        await db.execute(
            "CREATE TABLE userdata(id INTEGER PRIMARY KEY, relatedentityid INTEGER, relatedentityrecordid INTEGER, signontime TEXT, encryptionkey TEXT, email TEXT, rolecode TEXT, areacode INTEGER, nationalitycode TEXT, stakeholdercitycode TEXT, stakeholdernameen TEXT, creationsource TEXT, roleid INTEGER, applicantclassification TEXT, login TEXT, locationid TEXT, languageid TEXT, Urltoken TEXT, jobtitlecode TEXT, language TEXT, rolegroup TEXT, forgetpasswordcount INTEGER, stakeholderaddress TEXT, relatedentityname TEXT, jobtitle TEXT, statuscode TEXT, encryptedrelatedentityrecordid TEXT, stakeholderstatecode TEXT, stakeholdergoverneratecode TEXT, name TEXT, stakeholdernamear TEXT, locationids INTEGER, qrlink TEXT, crnnn TEXT, islogin INTEGER, mobile TEXT)"
        );
        return db;
      },
      version: 2,
    );
  }

  Future<void> insertUser(UserData userData) async {
    Database _db = await database();
    await _db.insert('userdata', userData.tomap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List> getUserData() async {
    Database _db = await database();
    List<Map<String, dynamic>> userdataMap =
    await _db.query('userdata');
    List data=[userdataMap[0]['encryptedrelatedentityrecordid'], userdataMap[0]['stakeholdernamear'],userdataMap[0]['stakeholdernameen']];
    return data;}

  Future<void> deleteUser(int id) async {
    Database _db = await database();
    await _db.rawDelete("DELETE FROM userdata WHERE id='$id'");
  }
}
