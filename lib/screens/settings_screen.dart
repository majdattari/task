import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majd_task/database/database.dart';
import 'package:majd_task/provider/provider.dart';
import 'package:majd_task/screens/login_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  bool notification = false;
  bool lang;

  Future<void> changeLang(bool val) async {
    SharedPreferences appLang = await SharedPreferences.getInstance();
    appLang.setBool('lang', val);
    setState(() {
      lang = appLang.getBool('lang');
    });
  }

  Future<void> getCurrentLang() async {
    SharedPreferences appLang = await SharedPreferences.getInstance();
    setState(() {
      lang = appLang.getBool('lang') ?? false;
    });
  }

  @override
  void initState() {
    getCurrentLang();
    if (lang == null)
      lang = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: lang ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          title: Text(lang ? "الاعدادات" : "Settings"),
          backgroundColor: Colors.blueGrey,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(children: [
                      Icon(
                        Icons.notifications,
                        color: Colors.black,
                      ),
                      SizedBox(width: 26,),
                      Text(lang ? "ايقاف الاشعارات" : "Disable Notification"),
                    ],),
                    Switch(value: notification, onChanged: (val) {
                      setState(() {
                        notification = val;
                      });
                    })
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(children: [
                      Icon(
                        Icons.language,
                        color: Colors.black,
                      ),
                      SizedBox(width: 26,),
                      Text(lang
                          ? "تحويل اللغه الى الانجليزية"
                          : "Change Language to arabic"),
                    ],),
                    Switch(value: lang, onChanged: (val) {
                      changeLang(val);
                      setState(() {
                        lang = val;
                      });
                    }),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Divider(),
                ListTile(
                  trailing: Icon(
                    Icons.logout,
                    color: Colors.black,
                  ),
                  title: Text(lang ? "تسجيل خروج" : "Logout"),
                  onTap: () async {
                    try{

                      MyDatabase _myDatabase = MyDatabase();
                      await _myDatabase.deleteUser(0);
                      print("user data deleted from database");

                      Provider.of<MyProvider>(context, listen: false)
                          .deleteUser();

                      SharedPreferences appLang = await SharedPreferences.getInstance();
                      appLang.setBool('userlogin', false);

                    }catch(e){
                      print(e.toString());
                    }finally{
                      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_){return LoginScreen();}));
                    }


                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


