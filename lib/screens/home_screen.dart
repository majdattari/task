import 'package:flutter/material.dart';
import 'package:majd_task/database/database.dart';
import 'package:majd_task/screens/settings_screen.dart';
import 'package:majd_task/widgets/card_builder.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool currentLang;
  MyDatabase _myDatabase = MyDatabase();

  Future<void> getLang() async {
    SharedPreferences appLang = await SharedPreferences.getInstance();
    setState(() {
      currentLang = appLang.getBool('lang') ?? false;
    });
  }


  @override
  void initState() {
    getLang();
    if (currentLang == null)
      currentLang = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: currentLang ? TextDirection.rtl : TextDirection.ltr,
      child: Scaffold(
        appBar: AppBar(
          title: Text(currentLang ? "الصفحة الرئيسية" : "Home"),
          backgroundColor: Colors.blueGrey,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton(icon: Icon(Icons.settings, color: Colors.white,
              ), onPressed: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) => SettingsScreen())).then((value) {
                  setState(() {
                    getLang();
                  });
                });
              }),
            )
          ],
        ),
        body:Container(
          color: Colors.grey[300],
          child: FutureBuilder(
              initialData: [],
              future: _myDatabase.getUserData(),
              builder: (context, snap){
                if(snap.connectionState==ConnectionState.waiting)
                  return Center(child: CircularProgressIndicator());
                if(snap.hasError)
                  return Center(child: Text("No Data"));
                else
                  return ListView.builder(
                    itemCount: 5,
                    itemBuilder: (context, index){
                      return InkWell(
                          onTap: () async {},
                          child: CardBuilder(currentLang, snap.data[0], snap.data[1], snap.data[2]));
                    },
                  );}
          ),
        ),
      ),
    );
  }
}
