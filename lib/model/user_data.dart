import 'package:flutter/material.dart';

class UserData {
  final int related_entity_id;
  final int related_entity_record_id;
  final String signontime;
  final String encryptionkey;
  final int user_id;
  final String email;
  final String role_code;
  final int area_code;
  final String nationality_code;
  final String stakeholdercitycode;
  final String stakeholdernameen;
  final String creation_source;
  final int role_id;
  final String applicant_classification;
  final String login;
  final String location_id;
  final String language_id;
  String Urltoken ="";
  final String job_title_code;
  final String language;
  final String role_group;
  final int forget_password_count;
  final String stakeholderaddress;
  final String related_entity_name;
  final String job_title;
  final String status_code;
  final String encrypted_related_entity_record_id;
  final String stakeholderstatecode;
  final String stakeholdergoverneratecode;
  final String name;
  final String stakeholdernamear;
  final int location_ids;
  final String qr_link;
  final String crn_nn;
  final int is_login;
  final String mobile;

  UserData({@required this.related_entity_id,
    @required this.related_entity_record_id,
    @required this.signontime,
    @required this.encryptionkey,
    @required this.user_id,
    @required this.email,
    @required this.role_code,
    @required this.area_code,
    @required this.nationality_code,
    @required this.stakeholdercitycode,
    @required this.stakeholdernameen,
    @required this.creation_source,
    @required this.role_id,
    @required this.applicant_classification,
    @required this.login,
    @required this.location_id,
    @required this.language_id,
    @required this.Urltoken,
    @required this.job_title_code,
    @required this.language,
    @required this.role_group,
    @required this.forget_password_count,
    @required this.stakeholderaddress,
    @required this.related_entity_name,
    @required this.job_title,
    @required this.status_code,
    @required this.encrypted_related_entity_record_id,
    @required this.stakeholderstatecode,
    @required this.stakeholdergoverneratecode,
    @required this.name,
    @required this.stakeholdernamear,
    @required this.location_ids,
    @required this.qr_link,
    @required this.crn_nn,
    @required this.is_login,
    @required this.mobile});

  factory UserData.fromJson(Map<String, dynamic> jsonData){
    return UserData(
        related_entity_id: jsonData['related_entity_id'],
        related_entity_record_id: jsonData['related_entity_record_id'],
        signontime: jsonData['signontime'],
        encryptionkey: jsonData['encryptionkey'],
        user_id: jsonData['user_id'],
        email: jsonData['email'],
        role_code: jsonData['role_code'],
        area_code: jsonData['area_code'],
        nationality_code: jsonData['nationality_code'],
        stakeholdercitycode: jsonData['stakeholdercitycode'],
        stakeholdernameen: jsonData['stakeholdernameen'],
        creation_source: jsonData['creation_source'],
        role_id: jsonData['role_id'],
        applicant_classification: jsonData['applicant_classification'],
        login: jsonData['login'],
        location_id: jsonData['location_id'],
        language_id: jsonData['language_id'],
        Urltoken: jsonData['urltoken']??"",
        job_title_code: jsonData['job_title_code'],
        language: jsonData['language'],
        role_group: jsonData['role_group'],
        forget_password_count: jsonData['forget_password_count'],
        stakeholderaddress: jsonData['stakeholderaddress'],
        related_entity_name: jsonData['related_entity_name'],
        job_title: jsonData['job_title'],
        status_code: jsonData['status_code'],
        encrypted_related_entity_record_id: jsonData['encrypted_related_entity_record_id'],
        stakeholderstatecode: jsonData['stakeholderstatecode'],
        stakeholdergoverneratecode: jsonData['stakeholdergoverneratecode'],
        name: jsonData['name'],
        stakeholdernamear: jsonData['stakeholdernamear'],
        location_ids: jsonData['location_ids'],
        qr_link: jsonData['qr_link'],
        crn_nn: jsonData['crn_nn'],
        is_login: jsonData['is_login'],
        mobile: jsonData['mobile']);
  }


  Map<String, dynamic> tomap() {
    return {
      "id": user_id,
      "relatedentityid": related_entity_id,
      "relatedentityrecordid": related_entity_record_id,
      "signontime": signontime,
      "encryptionkey": encryptionkey,
      "email": email,
      "rolecode": role_code,
      "areacode": area_code,
      "nationalitycode": nationality_code,
      "stakeholdercitycode": stakeholdercitycode,
      "stakeholdernameen": stakeholdernameen,
      "creationsource": creation_source,
      "roleid": role_id,
      "applicantclassification": applicant_classification,
      "login": login,
      "locationid": location_id,
      "languageid": language_id,
      "Urltoken": Urltoken,
      "jobtitlecode": job_title_code,
      "language": language,
      "rolegroup": role_group,
      "forgetpasswordcount": forget_password_count,
      "stakeholderaddress": stakeholderaddress,
      "relatedentityname": related_entity_name,
      "jobtitle": job_title,
      "statuscode": status_code,
      "encryptedrelatedentityrecordid": encrypted_related_entity_record_id,
      "stakeholderstatecode": stakeholderstatecode,
      "stakeholdergoverneratecode": stakeholdergoverneratecode,
      "name": name,
      "stakeholdernamear": stakeholdernamear,
      "locationids": location_ids,
      "qrlink": qr_link,
      "crnnn": crn_nn,
      "islogin": is_login,
      "mobile": mobile
    };
  }
}



