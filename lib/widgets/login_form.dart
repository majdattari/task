import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:majd_task/database/database.dart';
import 'package:majd_task/service/login_service.dart';
import 'package:majd_task/model/user_data.dart';
import 'package:majd_task/provider/provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginForm extends StatefulWidget{

  @override
  _LoginFormState createState() =>_LoginFormState();

}

class _LoginFormState extends State<LoginForm>{

  bool currentLang;

  Future<void> getLang() async {
    SharedPreferences appLang = await SharedPreferences.getInstance();
    setState(() {
      currentLang = appLang.getBool('lang') ?? false;
    });
  }

  Future<void> setDataTODatabse(UserData myuser) async {
    MyDatabase _myDatabase = MyDatabase();
    await _myDatabase.insertUser(myuser);
    print("user data added to database");
  }

  @override
  void initState() {
    getLang();
    if (currentLang == null)
      currentLang = false;
    super.initState();
  }

  final _formKey =GlobalKey<FormState>();
  String _user= "";
  String _password= "";
  bool _isLoad=false;

  Future _Login(String user, String password , BuildContext mycontext )async{

    try{
      setState(() {
        _isLoad=true;
      });
      Map result = await login(user, password);
      print("${result}");
      if(result['MESSAGE']=="LOGIN.SUCCESS"){

        Provider.of<MyProvider>(context, listen: false).addUser(result['DATA']);
        UserData myuser=Provider.of<MyProvider>(context, listen: false).user;

        await setDataTODatabse(myuser);

        SharedPreferences appLang = await SharedPreferences.getInstance();
        appLang.setBool('userlogin', true);

      }else
        throw currentLang ? "المستخدم غير صحيح":"incorrect user";
      print("done");
    }catch(e){
      String msg=e.toString();
      Scaffold.of(mycontext).showSnackBar(SnackBar(content: Text(msg)));
      setState(() {
        _isLoad=false;
      });
    }
  }

  void _submit(){
    final isValid =_formKey.currentState.validate();
    FocusScope.of(context).unfocus();

    if(isValid){
      _formKey.currentState.save();
      _Login(_user.trim(), _password.trim(), context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: currentLang ? TextDirection.rtl : TextDirection.ltr,
      child: Container(
        child: Center(
          child: Card(
            color:Colors.white,
            margin: EdgeInsets.symmetric(horizontal: 15),
            child: SingleChildScrollView(
              padding: EdgeInsets.all(20),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      textAlign: TextAlign.left,
                      autocorrect: false,
                      enableSuggestions: false,
                      textCapitalization: TextCapitalization.none,
                      key: ValueKey('User'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return currentLang ? 'الرجاء ادخال اسم مستخدم صحيح':'please enter a valid Username';
                        }
                        return null;
                      },
                      onSaved: (value)=>_user=value,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: currentLang ? "اسم المستخدم":"Username",
                      ),
                    ),
                    TextFormField(
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        labelText: currentLang ? "الرمز السري":"Password",
                      ),
                      key: ValueKey('password'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return currentLang ?'الرجاء ادخال رمز سري صحيح':'Please enter the correct password';
                        }
                        return null;
                      },
                      obscureText: true,
                      onSaved: (value)=>_password=value,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    OutlineButton(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
                      child:_isLoad?CircularProgressIndicator(): Text(
                        currentLang ? "دخول":'Login',
                        style: TextStyle(fontSize: 15),
                      ),
                      borderSide:
                      BorderSide(color: Colors.blueGrey),
                      onPressed: _submit, //_submit,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

}