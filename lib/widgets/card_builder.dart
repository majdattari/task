import 'package:flutter/material.dart';

class CardBuilder extends StatelessWidget {
  final bool currentlang;
  final String number;
  final String nameAr;
  final String nameEn;

  const CardBuilder(this.currentlang, this.number, this.nameAr, this.nameEn);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(currentlang?20:0),
              topRight: Radius.circular(currentlang?0:20),
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20))),
      elevation: 3,
      margin: EdgeInsets.only(left: 10, right: 10 , top: 10, bottom: 0),
      child: Container(
        padding: EdgeInsets.only(left: 7, right: 7 , top: 14, bottom: 7),

        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('assets/images/image.jpg',
                width: 75, height: 75),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(currentlang?"رقم الطلب":"Order Number", style: Theme.of(context).textTheme.body2,),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(child: Text("$number", style: Theme.of(context).textTheme.body1))
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Text(currentlang?"اسم المستثمر":"Stakeholder" ,style: Theme.of(context).textTheme.body2),
                      SizedBox(
                        width: 10,
                      ),
                      Flexible(child: Text(currentlang?"$nameAr":"$nameEn", style: Theme.of(context).textTheme.body1))
                    ],
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text(currentlang?"العدد المطلوب":"number" ,style: Theme.of(context).textTheme.body2),
                      SizedBox(
                        width: 10,
                      ),
                      Text("3", style: Theme.of(context).textTheme.body1)
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.access_time_outlined,
                        color: Colors.orangeAccent,
                        size: 35,
                      ),
                      Text("   "),
                      Text(currentlang?" قيد التدقيق":"Under scrutiny" ,style: Theme.of(context).textTheme.body2)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
